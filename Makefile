CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic -D _XOPEN_SOURCE=800

.PHONY: clean debug

TARGET=hangman
OBJS= hangman.o draw.o userInput.o stats.o

$(TARGET): $(OBJS)

debug: CFLAGS+=-g
debug: hangman

clean:
	rm hangman *.o
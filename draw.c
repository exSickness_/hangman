#include <stdio.h>
#include <string.h>

void drawGallow(char* dispTarget,int guessCount, char* remaining)
{
	// Function draws out the target word, gallow, and remaining guesses
	switch(guessCount)
	{
		case 0:
			printf("\t\t|______________________\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|______________________\n");

			printf("\n\t\t   Remaining Guesses:\n   ");
			for(int i = 0;i < (int) strlen(remaining); i++)
			{
				printf("%c ",remaining[i]);
			}
			printf("\n\n\t\t");
			for(int i = 0;i < (int)strlen(dispTarget);i++)
			{
				printf("%c ",dispTarget[i]);
			}
			printf("\n");
			break;
		case 1:
			printf("\t\t|___________________\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|             O\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|______________________\n");

			printf("\n\t\t   Remaining Guesses:\n   ");
			for(int i = 0;i < (int) strlen(remaining); i++)
			{
				printf("%c ",remaining[i]);
			}
			printf("\n\n\t\t");
			for(int i = 0;i < (int)strlen(dispTarget);i++)
			{
				printf("%c ",dispTarget[i]);
			}
			printf("\n");
			break;
		case 2:
			printf("\t\t|____________________\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|             O\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|______________________\n");

			printf("\n\n\t\t   Remaining Guesses:\n   ");
			for(int i = 0;i < (int) strlen(remaining); i++)
			{
				printf("%c ",remaining[i]);
			}
			printf("\n\t\t");
			for(int i = 0;i < (int)strlen(dispTarget);i++)
			{
				printf("%c ",dispTarget[i]);
			}
			printf("\n");
			break;
		case 3:
			printf("\t\t|___________________\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|             O\n");
			printf("\t\t|            /|\n");
			printf("\t\t|             |\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|______________________\n");

			printf("\n\t\t   Remaining Guesses:\n   ");
			for(int i = 0;i < (int) strlen(remaining); i++)
			{
				printf("%c ",remaining[i]);
			}
			printf("\n\n\t\t");
			for(int i = 0;i < (int)strlen(dispTarget);i++)
			{
				printf("%c ",dispTarget[i]);
			}
			printf("\n");
			break;
		case 4:
			printf("\t\t|___________________\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|             O\n");
			printf("\t\t|            /|\\\n");
			printf("\t\t|             |\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|______________________\n");

			printf("\n\t\t   Remaining Guesses:\n   ");
			for(int i = 0;i < (int) strlen(remaining); i++)
			{
				printf("%c ",remaining[i]);
			}
			printf("\n\n\t\t");
			for(int i = 0;i < (int)strlen(dispTarget);i++)
			{
				printf("%c ",dispTarget[i]);
			}
			printf("\n");
			break;
		case 5:
			printf("\t\t|___________________\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|             O\n");
			printf("\t\t|            /|\\\n");
			printf("\t\t|             |\n");
			printf("\t\t|            /\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|______________________\n");

			printf("\n\t\t   Remaining Guesses:\n   ");
			for(int i = 0;i < (int) strlen(remaining); i++)
			{
				printf("%c ",remaining[i]);
			}
			printf("\n\n\t\t");
			for(int i = 0;i < (int) strlen(dispTarget);i++)
			{
				printf("%c ",dispTarget[i]);
			}
			printf("\n");
			break;
		case 6:
			printf("\t\t|___________________\n");
			printf("\t\t|             |\n");
			printf("\t\t|             |\n");
			printf("\t\t|             O\n");
			printf("\t\t|            /|\\\n");
			printf("\t\t|             |\n");
			printf("\t\t|            / \\\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|\n");
			printf("\t\t|______________________\n");

			printf("\n\t\t   Remaining Guesses:\n   ");
			for(int i = 0;i < (int) strlen(remaining); i++)
			{
				printf("%c ",remaining[i]);
			}
			printf("\n\n\t\t");
			for(int i = 0;i < (int) strlen(dispTarget);i++)
			{
				printf("%c ",dispTarget[i]);
			}
			printf("\n");
			break;
	}
}

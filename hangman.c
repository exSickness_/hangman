#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <time.h>

#include "draw.h"
#include "stats.h"
#include "userInput.h"

int game();
void getWord(char* target,int argc, char *argv[]);


int main(int argc, char *argv[])
{
	// Main function to choose if they want to play or not.
	srand(time(NULL));
	char target[36];

	printf("\nWelcome to HANGMAN.\n");
	int loop = 1;
	char choice;
	char input[3];
	while(loop)
	{
		printf("\nCare to wager a life?\n");
		printf(" [Y]es\n [N]o\nMake your choice: ");
		
		fgets(input,4,stdin);

		choice = input[0];
		switch(choice)
		{
			case 'y':
			case 'Y':
			{
				printf("\n\n=======================================\n");
				printf("Let us begin.\n\n");
				getWord(target,argc,argv);
				game(target);
				break;
			}
			case 'n':
			case 'N':
			{
				loop = 0;
				printf("\n\nAnother day, then.\n");
				break;
			}
			default:
			{
				printf("Please enter a Y or an N.\n");
				printf("\n\n=======================================\n");
				if(input[1] != '\n')
				{
					while(!strchr(input, '\n'))
					{
						fgets(input, sizeof(input), stdin);
					}
					input[2] = '\0';
				}
				else
				{
					input[2] = '\0';
				}
				break;
			}
		}
	}
}

void getWord(char* target,int argc, char *argv[])
{
	// Function gets a target word
	char *line;
	FILE *fp;

	if(argc == 1)
	{
		char *homedir;
		if ((homedir = getenv("HOME")) == NULL)
		{
		    fprintf(stderr,"Could not find home directory.\n");
		    exit(1);
		}

		strcat(homedir,"/.words");
		printf("Reading word list from: %s\n",homedir);
		fp = fopen(homedir,"r");
		homedir[strlen(homedir) - 7] = '\0';
	}
	else if(argc == 2)
	{
		printf("Reading word list from: %s\n",argv[1]);
		fp = fopen(argv[1],"r");
	}
	else
	{
		fprintf(stderr,"Invalid number of arguments.\n");
		exit(2);
	}

	if(fp == NULL)
	{
		fprintf(stderr,"Failure to open word file.\n");
		exit(3);
	}

	int count = 0;
	size_t n = 0;
	// Count words in file
	while((getline(&line,&n,fp)) != -1)
	{
		count++;
	}
	if(count == 0)
	{
		fprintf(stderr,"Specified file is empty.\n");
		exit(4);
	}

	int r;
	selectWord:

	r = rand() % count;

	rewind(fp);

	// Select random word
	for(int i = 0; i < r;i++)
	{
		getline(&line,&n,fp);
	}

	// Check for invalid word
	if(strlen(line) <= 2 || strlen(line) >= 36)
	{
		goto selectWord;
	}

	line[strlen(line)] = '\0';
	strncpy(target ,line,36);

	free(line);
	fclose(fp);
}


int game(char* target)
{
	// Function pulls everything together.
	int correct = 0,guessCount = 0;
	char user[3] = "";
	char remaining[27] = "abcdefghijklmnopqrstuvwxyz";
	char dispTarget[20];

	int gameNum=0, wins=0,losses=0,average=0, elapsedT=0,totalTime=0;

	getStats(&gameNum,&wins,&losses,&average,&totalTime);

	time_t start = 0,end = 0;
    start = time(NULL);

    // Check to avoid floating point exception.
	if(average == 0)
	{
		printf("\nGame #%d  Wins: %d   Losses: %d     Average score: 0    Minutes: %d Seconds: %d\n",gameNum,wins,losses,totalTime/60, totalTime % 60);
	}
	else
	{
		printf("\nGame #%d  Wins: %d   Losses: %d     Average score: %.2f     Minutes: %d Seconds: %d\n",gameNum,wins,losses,((double)average / gameNum),totalTime/60, totalTime % 60);
	}

	size_t i = 0;
	// Loop for translating the letters in the target word to underscores.
	for (; i < strlen(target) -1; i++)
	{
		if((target[i] <= 'z' && target[i] >= 'a') || ( target[i] <= 'Z' && target[i] >= 'A') )
		{
			dispTarget[i] = '_';

		}
		else
		{
			dispTarget[i] = target[i];
		}
	}
	dispTarget[i] = '\0';

	// While the target still has unguessed letters.
	while(strchr(dispTarget, '_')) 
	{
		// If they have more than 6 guesses, the man chokes and dies.
		if(guessCount >6 ) 
		{
			break;
		}

		printf("\n\t\tIncorrect guesses: %d\n\n",guessCount);

		drawGallow(dispTarget,guessCount,remaining);

		userInput(user);
		
		// Idea from James McLaurin. Prints out remaining guesses.
		for (unsigned i = 0; i < strlen(remaining); i++)
		{
			if(remaining[i] == user[0])
			{
				remaining[i] = '-';
			}
		}

		int diff = 'A' - 'a';
		correct = 0;
		// Looping through target string
		for (unsigned i = 0; i < strlen(target); i++) 
		{
			// Check for user input matching uppercsae and lowercase target letters.
			if( (target[i] == user[0]) || (target[i] == (user[0] + diff)) ) 
			{
				dispTarget[i] = user[0];
				correct = 1;
			}
		}
		printf("\n");

		if(correct == 0)
		{
			guessCount++;
		}
		user[0] = '\0';
		printf("\n\n=======================================\n");
	}
	// Winner, winner, chicken dinner.
	if(!strchr(dispTarget, '_')) 
	{
		printf("You've won! The word was %s\nCongratulations!\n",target);
		end = time(NULL);
		elapsedT = (int)(end - start);
		printf("time elapsed: %d\n", elapsedT);
		wins++;
		gameNum++;
		average += guessCount;
		totalTime += elapsedT;
		setStats(&gameNum,&wins,&losses,&average,&totalTime);
		return(0);
	}
	
	// Loser.
	printf("You've lost! The word was %s. Better luck next time.\n",target); 
	end = time(NULL);
	elapsedT = (int)(end - start);
	printf("time elapsed: %d\n",elapsedT);
	losses++;
	gameNum++;
	totalTime += elapsedT;
	setStats(&gameNum,&wins,&losses,&average,&totalTime);
	return(0);
}




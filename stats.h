#ifndef stats_h
#define stats_h

void getStats(int* gameNum,int* wins,int* losses,int* average, int* totalTime);
void setStats(int* gameNum,int* wins,int* losses,int* average, int* totalTime);

#endif
#include <stdio.h>
#include <string.h>

void userInput(char* user)
{
	// Function handles user input for a guess.
	char c;

	while(user) // Loop until valid input is entered
	{
		printf("\n\t\t  Make a guess: ");
		fgets(user,3,stdin);
		
		if(user[1]!= '\n') // Checking for one character.
		{
			fprintf(stderr,"Please input one character.\n");
			if(strlen(user) < 2)
			{
				continue;
			}
			else
			{
				while ( (c=getchar()) != '\n' && c != EOF ) // Loop to clear out the stdin userfer.
    				/*Do nothing*/;
    			continue;
    		}
		}
		else // Entered one character plus a \n
		{
			if( (user[0] > 'z') || (user[0] < 'a') ) // Check for non number character
			{
				fprintf(stderr,"Please enter one alphabetic character.\n");
				continue; // Found a non-alpha character
				
			}
			user[1] = '\0';
			return;
		}	
	}
}
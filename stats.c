#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>

void getStats(int* gameNum,int* wins,int* losses,int* average,int* totalTime)
{
	// Function retrieves stats.
	char *line;
	char *buf = malloc(5 * sizeof(buf));
	FILE *fp;
	size_t n = 0;

	char *homedirS;
	if ((homedirS = getenv("HOME")) == NULL)
	{
	    fprintf(stderr,"Could not find home directory to retrieve stats.\n");
		   exit(1);
	}
	
	strcat(homedirS,"/.hangman");
	printf("Reading stats from: %s\n",homedirS);

	fp = fopen(homedirS,"r+");

	if(fp == NULL)
	{
		fprintf(stderr,"Failure to open stats file.\n");
		exit(2);
	}

	// File is empty.
	if(getline(&line,&n,fp) == -1)
	{
		char *c = "1\n0\n0\n0\n0\n";
		fprintf(fp,"%s",c);
		fprintf(stderr,"Empty file. Initializing stat file.\n");
	}

	rewind(fp);

	/* References for checking return value on strtol.
	 * http://stackoverflow.com/questions/11279767/how-do-i-make-sure-that-strtol-have-returned-successfully
	 * Man pages!
	 */

	char* endptr;

	getline(&buf,&n,fp);
	*gameNum = (int)strtol(buf,&endptr,10);
	if (endptr == buf)
	{
		// Nothing parsed from the string.
		fprintf(stderr,"Could not read game number. Initializing to 1.\n");
		*gameNum = 1;
	}

	getline(&buf,&n,fp);
	*wins = (int)strtol(buf,&endptr,10);
	if (endptr == buf)
	{
		// Nothing parsed from the string.
		fprintf(stderr,"Could not read number of wins. Initializing to 0.\n");
		*wins = 0;
	}

	getline(&buf,&n,fp);
	*losses = (int)strtol(buf,&endptr,10);
	if (endptr == buf)
	{
		// Nothing parsed from the string.
		fprintf(stderr,"Could not read number of losses. Initializing to 0.\n");
		*losses = 0;
	}

	getline(&buf,&n,fp);
	*average = (int)strtol(buf,&endptr,10);
	if (endptr == buf)
	{
		// Nothing parsed from the string.
		fprintf(stderr,"Could not read average. Initializing to 0.\n");
		*average = 0;
	}

	getline(&buf,&n,fp);
	*totalTime = (int)strtol(buf,&endptr,10);
	if (endptr == buf)
	{
		// Nothing parsed from the string.
		fprintf(stderr,"Could not read time. Initializing to 0.\n");
		*totalTime = 0;
	}

	free(line);
	free(buf);
	fclose(fp);
}




void setStats(int* gameNum,int* wins,int* losses,int* average, int* totalTime)
{
	// Function updates stats.
	FILE *fp;

	char *homedirS;
	if ((homedirS = getenv("HOME")) == NULL)
	{
		fprintf(stderr,"Failure to find home directory to set stats.\n");
		exit(3);
	}

	printf("Updating stats to: %s\n",homedirS);

	fp = fopen(homedirS,"w+");
	homedirS[strlen(homedirS) - 9] = '\0';
	if(fp == NULL)
	{
		fprintf(stderr,"Failure to open stats file.\n");
		exit(4);
	}
	
	// Adding stats into file.
	fprintf(fp,"%d\n",*gameNum);
	fprintf(fp,"%d\n",*wins);
	fprintf(fp,"%d\n",*losses);
	fprintf(fp,"%d\n",*average);
	fprintf(fp,"%d\n",*totalTime);

	fclose(fp);
}